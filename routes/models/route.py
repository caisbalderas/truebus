from django.db import models
from django.utils.translation import ugettext as _
from django.template.defaultfilters import slugify
from django.db.models import Count, Min, Sum, Avg


class Route(models.Model):

    id = models.AutoField(db_column="id", primary_key=True)
    route_id = models.IntegerField(_('metro id'), unique=True, default=None)
    route_short_name = models.CharField(_('short name'), max_length=15, blank=True, null=True)
    route_long_name = models.CharField(_('long name'), max_length=50, blank=True, null=True)
    route_url = models.URLField(_('url'), blank=True, null=True)
    slug = models.SlugField(max_length=100)

    def __unicode__(self):
        return self.get_full_name()

    def get_full_name(self):
        """
        Return route short name and long name
        """
        full_name = '{shortname} {longname}'.format(shortname=self.route_short_name, longname=self.route_long_name)
        return full_name

    def save(self, *args, **kwargs):
        if not self.id:  # so that the slug isn't changed on update, less broken links
            self.slug = slugify(self.route_long_name)

        super(Route, self).save(*args, **kwargs)