from django.shortcuts import render
from django.views.generic import DetailView, ListView
from .models import Route
from ratings.models import RouteRating
from django.db.models import Avg


class RouteDetailView(DetailView):

    model = Route
    template_name = 'route_detail.html'
    context_object_name = 'route'

    def get_context_data(self, **kwargs):

        context = super(RouteDetailView, self).get_context_data(**kwargs)
        queryset = RouteRating.objects.filter(route=self.object)

        # Add average rating
        context['route_rating'] = queryset.aggregate(Avg('stars'))

        # Add most recent reviews
        context['recent_reviews'] = queryset.order_by('-date_posted')
        return context


class RouteListView(ListView):

    model = Route
    template_name = 'route_list.html'
    context_object_name = 'route_list'