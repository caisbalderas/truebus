from django.shortcuts import render
from routes.models import Route
from ratings.models import RouteRating


def home(request):
    context = {}
    context['reviews'] = RouteRating.objects.all().order_by('-date_posted')[:18]
    return render(request, 'index.html', context)
