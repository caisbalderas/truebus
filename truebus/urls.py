from django.conf.urls import patterns, include, url
from django.contrib import admin
from routes.views import RouteDetailView, RouteListView

urlpatterns = patterns('',
    url(r'^$', 'truebus.views.home', name='home'),
    url(r'^route/(?P<route_id>\d+)/(?P<slug>.+)/', RouteDetailView.as_view(), name='detail'),
    url(r'^routes/', RouteListView.as_view(), name='list'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
