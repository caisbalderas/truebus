import os, csv
from routes.models import Route


def run():
    """
    loads from GTFS routes.txt and loads data into the database
    Instances are either updated or created if don't already exist
    """
    BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    filename = "{base_path}/routes.txt".format(base_path=BASE_DIR)

    # GTFS route schema and information -- https://developers.google.com/transit/gtfs/reference#routes_fields
    using = ['route_id', 'route_short_name', 'route_long_name', 'route_url']
    gtfs_schema = []

    with open(filename, 'r') as csvfile:
        rows = csv.reader(csvfile, delimiter=',')
        first = True

        for row in rows:
            if not first:
                data = dict.fromkeys([key for key in using])  # empty placeholder dictionary
                data = {key: row[gtfs_schema.index(key)] for key in data.keys()}  # fill it in with data
                data['route_id'] = int(data['route_id'])  # id needs to be an integer

                try:
                    route = Route.objects.filter(route_id=data['route_id'])
                    route.update(**data)
                    route[0].save()
                except:
                    route = Route(**data)
                    route.save()
            else:
                gtfs_schema = row
                first = False