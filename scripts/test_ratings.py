from random import randint
from faker import Factory
from ratings.models import RouteRating
from routes.models import Route
from django.utils import timezone


def run():
    faker = Factory.create()
    route_count = Route.objects.count()

    for x in range(0, 1000):  # 1000 RATINGS!
        stars = randint(1, 5)
        comment = faker.text()
        date = timezone.make_aware(faker.date_time(), timezone.get_current_timezone())
        route_id = randint(1, route_count - 1)

        new_route = RouteRating(route=Route.objects.get(id=route_id), date_posted=date, comment=comment, stars=stars)
        new_route.save()