from django.db import models
from routes.models import Route
from django.utils.translation import ugettext as _
from django.utils import timezone


class RouteRating(models.Model):
    id = models.AutoField(db_column="route_rating_id", primary_key=True)
    route = models.ForeignKey(Route, related_name=_('route_ratings'), blank=False, null=False)
    stars = models.IntegerField(_('stars'), blank=False, default=0)
    comment = models.TextField(_('comment'), max_length=120, blank=True, null=True)
    date_posted = models.DateTimeField(_('date posted'), default=timezone.now)


class DriverRating(models.Model):
    pass